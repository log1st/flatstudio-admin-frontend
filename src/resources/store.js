'use strict';

import Vue from 'vue';

import Vuex from 'vuex';

Vue.use(Vuex);

let store = new Vuex.Store({
	state: {
		xhrs: {},
		pageTitle: null,
		previousPageTitle: null,
		previousPageTitleReturn: null,
		previousRoute: null,
		action: null,
		projectLoadedOnce: false,
		api: {
			projects: {},
			projectsList: [],
			toggled: {
				published: true,
				archived: true
			}
		}
	}
});

store.watch((state) => {
	return state.pageTitle;
}, (newValue, oldValue) => {
	store.state.previousPageTitle = oldValue;
	document.querySelector('head title').innerHTML = newValue;
});


export default store;