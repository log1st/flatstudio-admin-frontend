'use strict';

import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import Portfolio from "../templates/pages/Portfolio/Portfolio.vue";
import Project from '../templates/pages/Project/Project.vue';

let router = new VueRouter({
	mode: 'history',
	routes: [
		{
			path: '/',
			name: 'index',
			redirect: {
				name: 'portfolio'
			}
		},
		{
			path: '/portfolio',
			name: 'portfolio',
			component: Portfolio
		},
		{
			path: '/portfolio/:id/:anchor?',
			name: 'project',
			component: Project
		},
		{
			path: '/api/:entity/:action?',
			name: 'api',
		},
		{
			path: '/action',
			name: 'action'
		}
	]
});

export default router;