'use strict';

let projects = {
	active: [],
	archive: []
};

import moment from 'moment';

let images = [
	'https://cdn.pixabay.com/photo/2017/08/30/01/05/milky-way-2695569_960_720.jpg',
	'https://images.pexels.com/photos/531880/pexels-photo-531880.jpeg?auto=compress&cs=tinysrgb&h=350',
	'https://images.pexels.com/photos/370799/pexels-photo-370799.jpeg?auto=compress&cs=tinysrgb&h=350',
	'https://media.istockphoto.com/photos/triangular-abstract-background-picture-id624878906',
	'http://images.all-free-download.com/images/graphiclarge/blue_abstract_background_310971.jpg',
	'https://image.freepik.com/free-vector/coloured-summer-background_1048-2276.jpg',
	'https://htmlcolorcodes.com/assets/images/html-color-codes-color-tutorials-hero-00e10b1f.jpg',
	'https://steamcdn-a.akamaihd.net/steamcommunity/public/images/items/441870/72cd26e14b7fcba028206a06f10548c0d5174cfc.jpg',
	'https://s3.amazonaws.com/Syntaxxx/background-gold-bokeh.jpg'
];

for(let i = 1; i <= 9; i++) {
	projects[i <= 7 ? 'active': 'archive'].push({
		id: i,
		title: 'Work name',
		type: i === 1 ? 'Published' : (i <= 3 ? 'Draft' : (i <= 7 ? 'Published': 'Archived')),
		date: i > 1 && i <= 3 ? null : moment().unix(),
		primary: i === 1,
		image: images[i]
	})
}

export default {
	status: 200,
	response: projects
}