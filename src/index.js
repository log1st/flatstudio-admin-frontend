'use strict';


import 'jquery';
import 'jquery-ui-bundle';
import 'air-datepicker';
import 'air-datepicker/src/js/i18n/datepicker.en';

import Vue from 'vue';
import store from './resources/store';
import router from './resources/router';
import IndexTemplate from './templates/Container.vue';

import moment from 'moment';

moment.locale('en');

Vue.component('c-header', require('./templates/partials/Header/Header.vue').default);

import Ajax from './classes/Ajax';

Vue.mixin({
	methods: {
		fetchProjects() {
			new Ajax({
				url: this.$router.resolve({
					name: 'api',
					params: {
						entity: 'projects'
					}
				}).href,
				dataType: 'json',
			})
				.then(json => {
					if(json.status === 200) {
						this.$store.state.api.projectsList = json.response;
						
						let grouped = {};
						
						for(let i in json.response) {
							if(!grouped.hasOwnProperty(json.response[i].status)) {
								grouped[json.response[i].status] = [];
							}
							
							grouped[json.response[i].status].push(json.response[i]);
						}
						
						this.$store.state.api.projects = grouped;
					}
				})
				.catch(res => {
					console.log(res);
				});
		}
	}
});

let Application = new Vue({
	el: '#app',
	components: {
		IndexTemplate
	},
	template: '<IndexTemplate/>',
	store,
	router
});

if (module.hot) {
	module.hot.accept();
}

export default Application;